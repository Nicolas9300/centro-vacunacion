package centroVacunacion;

public abstract class VacunaMenos18Grados extends Vacuna {
	
	public VacunaMenos18Grados(String nombreVacuna, Fecha _fechaIngreso) {
		super(nombreVacuna, _fechaIngreso);
	}

	@Override
	protected abstract boolean estaVencida();
}
