package centroVacunacion;

public class Astrazeneca extends Vacuna3Grados {

	public Astrazeneca(String nombreVacuna, Fecha fechaIngreso) {
		super(nombreVacuna,fechaIngreso);
	}

	@Override
	protected boolean estaVencida() {
		return false;
	}

	@Override
	public String toString() {
		return "nombre de Vacuna: " + super.nombre + ", fecha de ingreso: " + super.fechaIngreso;
	}
	

}
