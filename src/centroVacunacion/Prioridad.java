package centroVacunacion;

import java.util.ArrayList;


public class Prioridad {

	protected ArrayList<Persona> trabajadoresSalud;
	protected ArrayList<Persona> personasMayoresDe60;
	protected ArrayList<Persona> personasConPadecimientos;
	protected ArrayList<Persona> restoDelPublico;
	
	public Prioridad() {
		trabajadoresSalud = new ArrayList<Persona>();
		personasMayoresDe60 = new ArrayList<Persona>();
		personasConPadecimientos = new ArrayList<Persona>();
		restoDelPublico = new ArrayList<Persona>();
	}
	
	public void gestionarPorPrioridad(ArrayList<Persona> listaEspera) {
		for(Persona p: listaEspera) {
			if(p.trabajaEnSalud()) {
				trabajadoresSalud.add(p);
			}
			else if(p.edad() >= 60) {
				personasMayoresDe60.add(p);
			}
			else if(p.padeceEnfermedades()) {
				personasConPadecimientos.add(p);
			}
			else {
				restoDelPublico.add(p);
			}
		}
	}

	
	public Persona obtenerTrabajadorSalud() {
		Persona p = trabajadoresSalud.get(0);
		trabajadoresSalud.remove(0);
		return p;
	}

	public Persona obtenerPersonaMayor60() {
		Persona p = personasMayoresDe60.get(0);
		personasMayoresDe60.remove(0);
		return p;
	}

	public Persona obtenerPersonaConPadecimientos() {
		Persona p = personasConPadecimientos.get(0);
		personasConPadecimientos.remove(0);
		return p;
	}

	public Persona obtenerRestoPersonas() {
		Persona p = restoDelPublico.get(0);
		restoDelPublico.remove(0);
		return p;
	}
	
	public Persona obtenerPorPrioridad() {
		if(trabajadoresSalud.size() > 0 ) {
			return obtenerTrabajadorSalud();
		}
		else if(personasMayoresDe60.size() > 0) {
			return obtenerPersonaMayor60();
		}
		else if(personasConPadecimientos.size() > 0) {
			return obtenerPersonaConPadecimientos();
		}
		else {
			return obtenerRestoPersonas();
		}
	}

	
	
	
}
