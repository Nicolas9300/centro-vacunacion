package centroVacunacion;

public class Turno<Persona,Vacuna> {
	
	private Persona persona;
	private Vacuna vacuna;
	
	
	public Turno(Persona p, Vacuna v) {
		persona = p;
		vacuna = v;
		
	}

	public Persona obtenerPersona() {
		return persona;
	}
	
	public Vacuna obtenerVacuna() {
		return vacuna;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((persona == null) ? 0 : persona.hashCode());
		result = prime * result + ((vacuna == null) ? 0 : vacuna.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Turno))
			return false;
		Turno other = (Turno) obj;
		if (persona == null) {
			if (other.persona != null)
				return false;
		} else if (!persona.equals(other.persona))
			return false;
		if (vacuna == null) {
			if (other.vacuna != null)
				return false;
		} else if (!vacuna.equals(other.vacuna))
			return false;
		return true;
	}


	
}
