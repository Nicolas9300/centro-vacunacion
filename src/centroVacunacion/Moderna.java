package centroVacunacion;

public class Moderna extends VacunaMenos18Grados{
	
	public Moderna(String nombreVacuna, Fecha fechaIngreso) {
		super(nombreVacuna,fechaIngreso);
	}


	@Override
	protected boolean estaVencida() {
		Fecha f = new Fecha(fechaIngreso.dia(),fechaIngreso.mes(),fechaIngreso.anio());
		f.avanzarNDias(60);
		if(Fecha.hoy().posterior(f)) {
			return true;
		}
		return false;
	}
	
	@Override
	public String toString() {
		return "nombre de Vacuna: " + super.nombre + ", fecha de ingreso: " + super.fechaIngreso;
	}
	
}
