package centroVacunacion;

public abstract class Vacuna3Grados extends Vacuna {

	public Vacuna3Grados(String nombreVacuna, Fecha _fechaIngreso) {
		super(nombreVacuna, _fechaIngreso);
	}
	
	
	@Override
	protected abstract boolean estaVencida();
	
}
