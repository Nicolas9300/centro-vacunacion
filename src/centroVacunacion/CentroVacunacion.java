package centroVacunacion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class CentroVacunacion {
	
	private String nombre;
	private int capacidadDeVacunasDiaria;
	private HashMap<Vacuna,Integer> vacunasAlmacenadas;
	private ArrayList<Persona> listaEspera;
	private HashMap<Fecha,ArrayList<Turno>> turnos;
	private HashMap<Integer,String> reporteVacunados;
	private Map<String, Integer> vacunasVencidas;
	private Turno turno;
	private Prioridad prioridad;
	
	
	public CentroVacunacion(String _nombre, int capacidad) {
		if(_nombre.equals("")) {
			throw new RuntimeException("Nombre inv�lido.Intente nuevamente");
		}
		if(capacidad < 0) {
			throw new RuntimeException("Cantidad negativa.Intente nuevamente con un valor > 0");
		}
		nombre = _nombre;
		capacidadDeVacunasDiaria = capacidad;
		vacunasAlmacenadas = new HashMap<Vacuna,Integer>();
		reporteVacunados = new HashMap<Integer,String>();
		listaEspera = new ArrayList<Persona>();
		turno = new Turno<Persona,Vacuna>(null,null);
		turnos = new HashMap<Fecha,ArrayList<Turno>>();
		prioridad = new Prioridad();
		vacunasVencidas = new HashMap<String,Integer>();
	}
	
	public CentroVacunacion() {
	}

	public void ingresarVacunas(String nombreVacuna, int cantidad, Fecha fechaIngreso) {
		if(cantidad <= 0) {
			throw new RuntimeException("Cantidad inv�lida.Ingrese nuevamente con una cantidad v�lida");
		}
		else {
			if(nombreVacuna.equalsIgnoreCase("Pfizer")) {
				Vacuna v = new Pfizer(nombreVacuna,fechaIngreso);
				if(vacunasAlmacenadas.containsKey(v)) {
					if(v.getFechaIngreso().equals(fechaIngreso)) {
						vacunasAlmacenadas.put(v, vacunasAlmacenadas.get(v)+cantidad);
					}
				}
				else {
					vacunasAlmacenadas.put(v, cantidad);
				}
			}
			else if(nombreVacuna.equalsIgnoreCase("Sputnik")) {
				Vacuna v = new Sputnik(nombreVacuna,fechaIngreso);
				if(vacunasAlmacenadas.containsKey(v)) {
					if(v.getFechaIngreso().equals(fechaIngreso)) {
						vacunasAlmacenadas.put(v, vacunasAlmacenadas.get(v)+cantidad);
					}
				}
				else {
					vacunasAlmacenadas.put(v, cantidad);
				}
				
			}
			else if(nombreVacuna.equalsIgnoreCase("Sinopharm")) {
				Vacuna v = new Sinopharm(nombreVacuna,fechaIngreso);
				if(vacunasAlmacenadas.containsKey(v)) {
					if(v.getFechaIngreso().equals(fechaIngreso)) {
						vacunasAlmacenadas.put(v, vacunasAlmacenadas.get(v)+cantidad);
					}
				}
				else {
					vacunasAlmacenadas.put(v, cantidad);
				}
				
			}
			else if(nombreVacuna.equalsIgnoreCase("Moderna")) {
				Vacuna v = new Moderna(nombreVacuna,fechaIngreso);
				if(vacunasAlmacenadas.containsKey(v)) {
					if(v.getFechaIngreso().equals(fechaIngreso)) {
						vacunasAlmacenadas.put(v, vacunasAlmacenadas.get(v)+cantidad);
					}
				}
				else {
					vacunasAlmacenadas.put(v, cantidad);
				}
			}
			else if(nombreVacuna.equalsIgnoreCase("Astrazeneca")) {
				Vacuna v = new Astrazeneca(nombreVacuna,fechaIngreso);
				if(vacunasAlmacenadas.containsKey(v)) {
					if(v.getFechaIngreso().equals(fechaIngreso)) {
						vacunasAlmacenadas.put(v, vacunasAlmacenadas.get(v)+cantidad);
					}
				}
				else {
					vacunasAlmacenadas.put(v, cantidad);
				}
			}
			else {
				throw new RuntimeException("Vacuna inadmisible.Ingrese una vacuna soportada por el centro.");
			}
		}
	}

	public void inscribirPersona(int dni, Fecha nacimiento, boolean tienePadecimientos, boolean esEmpleadoSalud) {
		Persona p = new Persona(dni,nacimiento,tienePadecimientos,esEmpleadoSalud);
		if(listaEspera.contains(p)) {
			throw new RuntimeException("Ya esta en lista de espera, por favor sea paciente.");
		}
		else if((Fecha.diferenciaAnios(Fecha.hoy(),nacimiento) < 18)) {
			throw new RuntimeException("No cumple con el requisito de edad para vacunarse.");
		}
		else if(reporteVacunacion().containsKey(dni)) {
			throw new RuntimeException("La persona ya ha sido vacunada.");
		}
		listaEspera.add(p);
	}
	

	public void generarTurnos(Fecha fechaInicial) {
		prioridad.gestionarPorPrioridad(listaEspera);
		cancelarTurno();
		sacarVacunasVencidas();
		

		
		if(fechaInicial.posterior(Fecha.hoy()) ) {
			
			while(listaEspera.size() > 0) {
				
				for(int i = 0; i < capacidadDeVacunasDiaria; i++) {
					if(listaEspera.size() == 0) {
						break;
					}
					Persona p =  prioridad.obtenerPorPrioridad();
					Vacuna v;
					if(p.trabajaEnSalud()) {
						 v = obtenerVacunaPersonalSalud();
						 descontarVacuna(v);
					}
					else if(p.edad() >= 60) {
						 v = obtenerVacunaMayor60();
						 descontarVacuna(v);
					}
					else if(p.padeceEnfermedades()) {
						 v = obtenerVacunasParaPadecientes();
						 descontarVacuna(v);
					}
					else {
						 v = obtenerVacunaParaResto();
						 descontarVacuna(v);
					}
					turno = new Turno<Persona,Vacuna>(p,v);
					if(turnos.get(fechaInicial) == null) {
						turnos.put(fechaInicial, new ArrayList<Turno>());
						turnos.get(fechaInicial).add(turno);
					}
					else {
						turnos.get(fechaInicial).add(turno);
					}
					listaEspera.remove(p);
					if(i == 4) {
						Fecha f = new Fecha(fechaInicial.dia(),fechaInicial.mes(),fechaInicial.anio());
						f.avanzarUnDia();
						fechaInicial = f;
					}
				}
				
			}	
		}
		else {
			throw new RuntimeException("Fecha invalida");
		}
	}


	private void sacarVacunasVencidas() {
		Iterator<Vacuna> it = vacunasAlmacenadas.keySet().iterator();
		while(it.hasNext()) {
			Vacuna v = it.next();
			if(v.estaVencida()) {
				vacunasVencidas.put(v.getNombre(), vacunasAlmacenadas.get(v));
				it.remove();
			}
		}
	}

	private Vacuna obtenerVacunaParaResto() {
		if(vacunasDisponibles("Sinopharm") > 0) {
			return obtenerVacuna("Sinopharm");
		}
		else if(vacunasDisponibles("Moderna") > 0) {
			return obtenerVacuna("Moderna");
		}
		else {
			return obtenerVacuna("Astrazeneca");
		}
	}

	private Vacuna obtenerVacunasParaPadecientes() {
		if(vacunasDisponibles("Sinopharm") > 0) {
			return obtenerVacuna("Sinopharm");
		}
		else if(vacunasDisponibles("Moderna") > 0) {
			return obtenerVacuna("Moderna");
		}
		else{
			if(vacunasDisponibles("Astrazeneca") > 0) {
				return obtenerVacuna("Astrazeneca");
			}
			return null;
		}	
	}

	private Vacuna obtenerVacunaMayor60() {
		if(vacunasDisponibles("Sputnik") > 0) {
			return obtenerVacuna("Sputnik");
		}
		else{
			if(vacunasDisponibles("Pfizer") > 0) {
				return obtenerVacuna("Pfizer");
			}
			return null;
		}
	}

	private Vacuna obtenerVacunaPersonalSalud() {
		if(vacunasDisponibles("Sinopharm") > 0) {
			return obtenerVacuna("Sinopharm");
		}
		else if(vacunasDisponibles("Moderna") > 0) {
			return obtenerVacuna("Moderna");
		}
		else{
			if(vacunasDisponibles("Astrazeneca") > 0) {
				return obtenerVacuna("Astrazeneca");
			}
			return null;
		}
	}

	private void descontarVacuna(Vacuna v) {
		Iterator it = vacunasAlmacenadas.entrySet().iterator();
		while(it.hasNext()) {
			Map.Entry par = (Map.Entry)it.next();
			if(par.getKey().equals(v) && (int)par.getValue() > 0) {
				vacunasAlmacenadas.put(v, vacunasAlmacenadas.get(v)-1);
			}
		}
	}

	private void cancelarTurno() {
		Iterator<Fecha> it = turnos.keySet().iterator();
		while(it.hasNext()) {
			Fecha fecha = it.next();
			if(fecha.anterior(Fecha.hoy())) {
				devolverVacuna(turnos.get(fecha));
				it.remove();
			}
		}				
	}

	private void devolverVacuna(ArrayList<Turno> turns) {
		for(Turno t: turns) {
			Vacuna v = (Vacuna) t.obtenerVacuna();
			if(vacunasAlmacenadas.containsKey(v)) {
				vacunasAlmacenadas.put(v, vacunasAlmacenadas.get(v)+1);
			}
			else {
				vacunasAlmacenadas.put(v, 1);
			}
					
		}
	}

	public List<Integer> turnosConFecha(Fecha fecha) {
		List<Integer> cantidadTurnosPorFecha = new ArrayList<Integer>();
		if(!turnos.containsKey(fecha)) {
			return cantidadTurnosPorFecha;
		}
		else {
			ArrayList<Turno> turnosFecha = turnos.get(fecha);

		    for (Turno t : turnosFecha) {
		    	Persona p = (Persona) t.obtenerPersona();
		    	cantidadTurnosPorFecha.add(p.getDni());
		    }
		    return cantidadTurnosPorFecha;
		}
		
	}

	public void vacunarInscripto(int dni, Fecha fechaVacunacion) {
		if(!turnos.containsKey(fechaVacunacion)) {
			throw new RuntimeException("No esta habilitado para vacunarse");
		}
		else {
			ArrayList<Turno> listaTurnos = turnos.get(fechaVacunacion);
			
			Iterator<Turno> it = listaTurnos.iterator();
			while(it.hasNext()) {
				Turno t = it.next();
				Persona p = (Persona) t.obtenerPersona();
				Vacuna v = (Vacuna) t.obtenerVacuna();
				if(dni == p.getDni() && v != null) {
					reporteVacunados.put(p.getDni(),v.getNombre());
					it.remove();
				}
				
			}
		}
		
		
	}

	public int vacunasDisponibles() {
		int cantidad = 0;
		Iterator it = vacunasAlmacenadas.entrySet().iterator();
		while(it.hasNext()) {
			Map.Entry par = (Map.Entry)it.next();
			cantidad = cantidad + (int)par.getValue();
		}
		return cantidad;
	}


	public List<Integer> listaDeEspera() {
		List<Integer> listaDnis = new ArrayList<Integer>();
		for(Persona p: listaEspera) {
			listaDnis.add(p.getDni());
		}
		return listaDnis;
	}

	public Map<Integer,String> reporteVacunacion() {
		return reporteVacunados;
	}

	public Map<String,Integer> reporteVacunasVencidas() {
		return vacunasVencidas;
	}

	public int vacunasDisponibles(String nombreVacuna) {
		int cont = 0;
		Iterator<Vacuna> it = vacunasAlmacenadas.keySet().iterator();
		while(it.hasNext()) {
			Vacuna v = it.next();
			if(v.getNombre().equalsIgnoreCase(nombreVacuna)) {
				cont = cont + vacunasAlmacenadas.get(v);
			}
		}
		return cont;
		
	}

	public Vacuna obtenerVacuna(String nombreVacuna) {
		for(Vacuna v : vacunasAlmacenadas.keySet()) {
			if(v.getNombre().equalsIgnoreCase(nombreVacuna)) {
				return v;
			}
		}
		return null;
	}
	
	private int totalTurnosAsignados() {
		int cont = 0;
		for(Fecha f: turnos.keySet()) {
			cont = cont + turnos.get(f).size();
		}
		return cont;
	}
	
	private int totalVacunasAplicadas() {
		int cont = 0;
		for(Integer i : reporteVacunados.keySet()) {
			cont++;
		}
		return cont;
	}
	
	@Override
	public String toString() {
		return "Nombre: " + nombre + "\ncapacidad diaria: " + capacidadDeVacunasDiaria
				+ "\ncant vacunas disponibles: " + vacunasDisponibles() + "\ncant de personas en espera: " +
				listaDeEspera().size() + "\ncant turnos asignados: " + totalTurnosAsignados() + "\ncant vacunas aplicadas: "+
				totalVacunasAplicadas();
	}

	
	
}
