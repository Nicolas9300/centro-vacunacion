package centroVacunacion;

public abstract class Vacuna {
	
	protected String nombre;
	protected Fecha fechaIngreso;
	
	public Vacuna(String nombreVacuna,Fecha _fechaIngreso) {
		nombre = nombreVacuna;
		fechaIngreso = _fechaIngreso;
	}


	protected abstract boolean estaVencida();
	
	
	public String getNombre() {
		return nombre;
	}
	
	public Fecha getFechaIngreso() {
		return fechaIngreso;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fechaIngreso == null) ? 0 : fechaIngreso.hashCode());
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!(obj instanceof Vacuna))
			return false;
		Vacuna other = (Vacuna) obj;
		if (fechaIngreso == null) {
			if (other.fechaIngreso != null)
				return false;
		} else if (!fechaIngreso.equals(other.fechaIngreso))
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		return true;
	}

	@Override
	abstract public String toString();


	
	

	
}
