package centroVacunacion;

public class Persona {
	
	private int dni;
	private Fecha fechaDeNacimiento;
	private boolean tienePadecimientos;
	private boolean esEmpleadoSalud;
	
	public Persona(int _dni, Fecha nacimiento, boolean _tienePadecimientos, boolean _esEmpleadoSalud) {
		dni = _dni;
		fechaDeNacimiento = nacimiento;
		tienePadecimientos = _tienePadecimientos;
		esEmpleadoSalud = _esEmpleadoSalud;
	}
	
	public int getDni() {
		return dni;
	}

	public boolean trabajaEnSalud() {
		return esEmpleadoSalud;
	}
	
	public int edad() {
		return Fecha.diferenciaAnios(Fecha.hoy(), fechaDeNacimiento);
	}
	
	public boolean padeceEnfermedades() {
		return tienePadecimientos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + dni;
		result = prime * result + (esEmpleadoSalud ? 1231 : 1237);
		result = prime * result + ((fechaDeNacimiento == null) ? 0 : fechaDeNacimiento.hashCode());
		result = prime * result + (tienePadecimientos ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Persona))
			return false;
		Persona other = (Persona) obj;
		if (dni != other.dni)
			return false;
		if (esEmpleadoSalud != other.esEmpleadoSalud)
			return false;
		if (fechaDeNacimiento == null) {
			if (other.fechaDeNacimiento != null)
				return false;
		} else if (!fechaDeNacimiento.equals(other.fechaDeNacimiento))
			return false;
		if (tienePadecimientos != other.tienePadecimientos)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "DNI: " + dni + "\nFecha de nacimiento: " + fechaDeNacimiento +
				"\nPadece enfermedades: " + tienePadecimientos + "\nEs empleado de salud: " +
				esEmpleadoSalud;
	}
	
}
